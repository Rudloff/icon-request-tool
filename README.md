![CI bagde](https://gitlab.com/xphnx/icon-request-tool/badges/master/build.svg)

# *DEPRECATED*


# Turtl - TwelF Unthemed Request Tool

With this app you can share via email (or other protocols like Owncloud, NFC, Bluetooth, IM...) the data for adding the icons missing in your icon pack. It just collects the app title, package name, activity name and the icon file of the apps your select. By default have set the email address for [TwelF/Onze Theme](https://f-droid.org/repository/browse/?fdid=org.twelf.cmtheme) requests.

## Preview

![turtl_screenshot2](/uploads/f8c0cb7fe75dca41c47f152af7cf2d28/turtl_screenshot2.png)

## Installation

[![Get_it_on_F-Droid.svg](https://gitlab.com/uploads/xphnx/twelf_cm12_theme/a4649863bd/Get_it_on_F-Droid.svg.png)](https://f-droid.org/app/org.xphnx.iconsubmit)

## Permissions

    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

These permissions are needed. Otherwise you wont be able to create the zip files.

## License

[<img src="http://gplv3.fsf.org/gplv3-127x51.png" />](http://www.gnu.org/licenses/gpl-3.0.html)

